###################################################################################
#Matt Hibberd, 9/26/2014                                                          #
#                                                                                 #
#140930:  changed "base" extract to ignore leading path information               #
#         if inputting usr-formatted data, force the first column name to "Time"  #
#150309:  allowed for Temp variable in input data, but dropped from df before melt#
#150310:  Added subset of rawdata to wells specified in meta, to avoid warnings   #
#         at the inner_join in line 63                                            #
#151029:  Mods to allow interpolation of missing points and dashed line plotting  #
#           of any missing data                                                   #
###################################################################################

library(methods)
library(reshape2)
library(dplyr, lib.loc="/home/hibberdm/scratch/growth_curves/R-packages")
library(naturalsort, lib.loc="/home/hibberdm/scratch/growth_curves/R-packages")
#suppressPackageStartupMessages(suppressWarnings(library(tcltk)))
library(ggplot2)

###Functions###

sem <- function(x) sd(x)/sqrt(length(x))

time_convert <- function(x) {
  split <- strsplit(x, ":")
  proc <- as.numeric(split[[1]][1]) + as.numeric(split[[1]][2])/60 + as.numeric(split[[1]][3])/3600
  return(proc)
}

cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#0072B2", "#D55E00", "#CC79A7", "#000000")

###Set wd, get passed arguments###

setwd("./")
argv <- commandArgs(TRUE)

f <- argv[1]
m <- argv[2]

threshold <- as.numeric(argv[3])
base <- sub("^.*/([^.]*).*", "\\1", f) #get base name for output file naming
usr_format <- argv[4]
if (usr_format == "T") {convert_times <- "F"} else {convert_times <- "T"}
header_lines <- as.numeric(argv[5])
number_datapoints <- as.numeric(argv[6])

if (usr_format == "T") {
  rawdata = read.table(f, nrows = number_datapoints, sep="\t", header=T)
  colnames(rawdata)[1] <- "Time"
} else {
  rawdata = read.table(f, skip = header_lines, nrows = number_datapoints, sep="\t", header=T)
  }
meta = read.table(m, sep="\t", header=T)

#Cuts rawdata to those wells specified in meta
wells_to_analyze <- as.character(meta$Well)

###data manip and summary (from files)###
if (usr_format == "T") {
	rawdata_clean <- rawdata
} else {
#	rawdata_clean <- subset(rawdata, select = -c(T.OD600.600))  
	rawdata_clean <- rawdata[, !(names(rawdata) %in% c("T.OD600.600", "T.600"))]
	}
#subsetting this way avoids warnings from inner_join below
rawdata_sub <- subset(rawdata_clean, select = c(wells_to_analyze, "Time"))
rawdata_long <- melt(rawdata_sub, id="Time", variable.name="Well", value.name="OD600")
rawdata_long$Well <- factor(as.character(rawdata_long$Well))
rawdata_annotated <- inner_join(rawdata_long, meta, by="Well")
if (convert_times == "T") {
  rawdata_annotated["Time"] <- apply(rawdata_annotated["Time"], 1, time_convert)
}
treatments <- naturalsort(as.vector(unique(rawdata_annotated$Treatment)))
rawdata_annotated$Treatment <- factor(rawdata_annotated$Treatment, levels=c(treatments))
#organisms <- naturalsort(as.vector(unique(rawdata_annotated$Organism)))
organisms <- as.vector(unique(rawdata_annotated$Organism))
rawdata_annotated$Organism <- factor(rawdata_annotated$Organism, levels=c(organisms))
grouped_data <- group_by(rawdata_annotated, Organism, Medium, Treatment, Time)
stats <- summarise(grouped_data, N=length(OD600), MEAN=mean(OD600), SEM=sem(OD600))
#Interpolation Handling
#rawdata_annotated$Sort <- paste(rawdata_annotated$Organism, rawdata_annotated$Medium, rawdata_annotated$Treatment, sep="_")
#sort <- levels(factor(rawdata_annotated$Sort))
sort <- levels(factor(rawdata_annotated$Well))
temp_df <- rawdata_annotated[1,]
temp_df$interp <- 1
for (i in sort) {
  interp_sub <- subset(rawdata_annotated, rawdata_annotated$Well == i)
  interp_sub$interp <- with(interp_sub, interp1(interp_sub$Time, interp_sub$OD600, interp_sub$Time, method = "linear"))
  temp_df <- rbind(temp_df, interp_sub)
  }
rawdata_annotated <- temp_df[2:nrow(rawdata_annotated),]
yrange <- c(0,max(stats$MEAN)+max(stats$SEM))
xrange <- c(0,max(stats$Time))

###Plotting Stacked, Continuous Axis, individual wells###
pdf(file=paste(base, "_curve_plots_stacked_continuous_individual_interp.pdf", sep = ""))
ggplot(data=rawdata_annotated, aes(x=Time, y=OD600, color=Treatment, group=Well)) +
  #  theme(legend.position="none") +
  #  geom_ribbon(aes(ymin=MEAN-SEM,ymax=MEAN+SEM), colour=NA, alpha=0.3) +
  geom_line() +
  scale_colour_manual(values=cbPalette) +
  #  scale_colour_hue(l=50) +
  facet_grid(Organism ~ ., scales = "free_y") +
  labs(x="time (hr)", y="OD600") +
  ggtitle(base) +
  theme(plot.title=element_text(size=8, face="bold")) +
  theme(axis.title = element_text(size = 8, face="bold")) +
  #  scale_y_continuous(limits = yrange, breaks=seq(0, max(yrange), by=0.25)) +
  #  scale_y_log10() +
  scale_x_continuous(limits = xrange, breaks=seq(0, max(xrange), by=6)) +
  theme(panel.background=element_rect(colour="black", fill=NA)) +
  geom_hline(yintercept=threshold, colour = "blue", size=0.25, linetype = "dashed") +
  theme(strip.text.y=element_text(size=8, angle=0, vjust=0.5, hjust=0.5))  + 
  theme(strip.text.x=element_text(size=8, angle=0, vjust=0.5, hjust=0.5)) +
  theme(strip.background = element_rect(colour="black", fill="gray90")) +
  theme(panel.border=element_rect(colour="black", fill=NA)) +
  theme(axis.ticks=element_line(colour="black")) +
  theme(panel.background=element_rect(colour="black", fill=NA)) +
  #  theme(panel.grid.major.y=element_line(colour=NA)) +
  #  theme(panel.grid.major.x=element_line(colour=NA)) +
  theme(panel.grid.minor = element_blank()) +
  theme(axis.text.y=element_text(colour="black", size=8, vjust=0.5)) +
  theme(axis.text.x=element_text(colour="black", size=8, angle=-90, vjust=0.5))
dev.off()

warnings()
