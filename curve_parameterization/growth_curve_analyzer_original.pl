#!/usr/bin/perl -w

#growth_curve_analyzer.pl
#Calculates bacterial growth curve metrics from raw BioTek Eon output data
#
#Matt Hibberd, 140909
#150309 Updated to fix eon files and proceed with analysis
#150312 Question: should I implement a way to user-define the points to be analyzed
#					aside from modifying the input file?

use strict;
#System-wide perl modules
use Getopt::Long qw(:config no_ignore_case);
use List::Util qw(first max min);
use List::MoreUtils qw(firstidx uniq);
#MCH's locally installed perl modules
#use lib "/home/comp/jglab/mhibberd/scripts/lib_perl/";
use Statistics::Basic qw(:all);
use Statistics::R;
use Sort::Naturally qw(nsort);
use Number::Format qw(round);
use File::Basename;

#declaring variables
my $infile;
my $outfile;
my $groupfile;
#default settings for user-definable threshold & range.
my $log_threshold = 0.3;
my $range = 1;
#additional getopt options
my $format_flag;
my $help;
#declare data structures
my %meta_hash = ();
my %data_hash = ();
my %experiments = ();
my $datestring = localtime();
my $data_points = 0;
my $gap_lines = 0;
my @times = ();
my $plot_flag;
my $header_lines = 0;
my $CURVE_PLOTTER = "./plotting/growth_curve_plotter.R";
my $rate_window = 1;
my $fix_flag;
my $interpolate_flag;
my $maximum_time = 0;

GetOptions(
	"infile=s" => \$infile,
	"metadata=s" => \$groupfile,
	"outfile=s" => \$outfile,
	"threshold=s" => \$log_threshold,
	"range=i" => \$range,
	"window=i" => \$rate_window,
	"user_formatted" => \$format_flag,
	"plots" => \$plot_flag,
	"fix" => \$fix_flag,
	"gap" => \$interpolate_flag,
	"help" => \$help,
);

#show help and die if desired or unless input and group files are specified
show_help() if $help;
show_help("Error:  must pass required parameter -i") unless $infile;
show_help("Error:  must pass required parameter -m") unless $groupfile;

if ($fix_flag) {
	$infile = format_eon_file($infile);
#	die "Test -> check formatted file\n";
	}

if ($interpolate_flag) {
	$CURVE_PLOTTER = "./plotting/growth_curve_plotter_interp.R";
	}

#check file format
file_format_check($infile);

my ($basename, $dirs) = fileparse($infile);
$basename =~ s/\..*//;	#strip suffix from infile name for output use later

#print "My basename, dirs, suffix are:\n\t$basename\n\t$dirs\n\n";

unless ($outfile) { #generate output filename if not user-defined
	$outfile = "$basename"."_curve_metrics.txt";
	}

print "...starting curve analysis:\n";

#get metadata on experiment from input "groupfile"
if ($groupfile) {
	print "\tParsing experiment file: $groupfile\n";
	open (EXP, "<$groupfile") or die "ERROR: Can't open $groupfile: $!\n";
	while (my $line = <EXP>) {
		$line =~ s/\R//g;
		chomp $line;
		unless ($line =~ /^Well|^#/) {
			my @line_array = split ("\t", $line);
			$meta_hash{$line_array[0]}{"organism"} = $line_array[1];
			$meta_hash{$line_array[0]}{"medium"} = $line_array[2];
			$meta_hash{$line_array[0]}{"treatment"} = $line_array[3];
			$meta_hash{$line_array[0]}{"all"} = join("_", $line_array[1], $line_array[2], $line_array[3]);
			$experiments{join("_", $line_array[1], $line_array[2], $line_array[3])}++; #this becomes the main grouping variable
#			print "$meta_hash{$line_array[0]}{all}\n";
			}
		}
	close EXP;
	}

foreach my $key (nsort keys %experiments)	{
	print "\t\tGroup = $key, Reps = $experiments{$key}\n";
	}

my @sample_nums = map {$experiments{$_}} nsort keys %experiments;
my $unique = uniq @sample_nums;
my $clean_format = 0;
if ($unique == 1) {
	$clean_format++;
	}

if ($format_flag) { #if formatted files are input
	my ($tmp1, $tmp2, $tmp3, $tmp4, $tmp5) = read_formatted_input($infile);
	$data_points = $tmp1;
	%data_hash = %$tmp2;
	@times = @$tmp3;
	$gap_lines = $tmp4;
	$maximum_time = $tmp5;
	}
else {	#if raw (from Eon) files are provided
	my ($tmp1, $tmp2, $tmp3, $tmp4, $tmp5, $tmp6) = read_raw_input($infile);
	$data_points = $tmp1;
	%data_hash = %$tmp2;
	@times = @$tmp3;
	$header_lines = $tmp4;
	$gap_lines = $tmp5;
	$maximum_time = $tmp6;	
	}

my $data_lines = $data_points;
#my $data_lines = $data_points-1;
if ($data_lines < 1) {
	die "\n***No data lines found. Did you specify -f where you shouldn't have?***\n\n";
	}
else {
	print "\tHave $data_lines data points per curve (excluding header row)\n";
	print "\t\tData was not found for $gap_lines lines.  These lines were skipped for analysis (but not for plotting).\n";
	}

#extract curve metrics from data for each individual sample, and put them into HoHoA
print "\tCalculating curve metrics...\n";
foreach my $well (nsort keys %data_hash) {
	unless ($well =~ /time/) {
		my $pass_index = firstidx {$_ >= $log_threshold} @{$data_hash{$well}{"curve"}};
		if ($pass_index != -1) {
			$data_hash{$well}{log_od} =	@{$data_hash{$well}{"curve"}}[$pass_index];
			$data_hash{$well}{time_to_log} =	$times[$pass_index];
#			print "$well TTL: $data_hash{$well}{time_to_log}\n"
			}
		else {
			$data_hash{$well}{log_od} =	"NA";
			$data_hash{$well}{time_to_log} = "NA";
#			print "$well TTL: ${$data_hash{$well}{time_to_log}}\n"
			}
		my $max = max @{$data_hash{$well}{"curve"}};
		my $max_index = firstidx {$_ == $max} @{$data_hash{$well}{"curve"}};
#		print "My Well is $well, Max Index is $max_index\n";
		my $max_ave = get_range_ave($max_index, \@{$data_hash{$well}{"curve"}}, $range);
		$data_hash{$well}{max_ave} = $max_ave;
		$data_hash{$well}{time_to_max} = $times[$max_index];
#		print "$well MaxOD: $data_hash{$well}{max_ave}\n"
		my ($max_rate, $max_rate_time) = calculate_growth_rate(\@{$data_hash{$well}{curve}}, \@times, $rate_window);
		$data_hash{$well}{max_rate} = $max_rate;
		$data_hash{$well}{max_rate_time} = $max_rate_time;
		}
	}

#average (guided by exp group file) and output data
open (OUT, "+>$outfile") or die "ERROR: Can't open $outfile: $!\n";
print OUT "Input File: $infile\nGroup File: $groupfile\nDate: $datestring\nLog OD Threshold: $log_threshold\nRange: $range (max +/- n)\n";
printf OUT "Duration of Experiment: %.2f (hr)\n", $maximum_time;
print OUT "Group\tn\ttl_n\ttl(hr)\ttl_SD\tmaxod_n\tmaxod\tmaxod_SD\tmaxrate\tmaxrate_SD\t\ttl_reps";
my $out_first_flag = 0;
foreach my $exp (nsort keys %experiments) {
	my @wells = grep { $meta_hash{$_}{all} eq $exp } nsort keys %meta_hash;
	if (($out_first_flag > 0) && ($clean_format == 0)) {
		print OUT "\t\t\t\t\t\t\t\t\t\t\ttl_reps";
		}
	if ($clean_format == 0) {
		for (my $i = 0; $i < @wells; $i++) {
			print OUT "\t";
			}
		print OUT "maxod_reps";
		for (my $i = 0; $i < @wells; $i++) {
			print OUT "\t";
			}
		print OUT "maxrate_reps\n";
		}
	elsif (($clean_format > 0) && ($out_first_flag == 0)) {
		for (my $i = 0; $i < @wells; $i++) {
			print OUT "\t";
			}
		print OUT "maxod_reps";
		for (my $i = 0; $i < @wells; $i++) {
			print OUT "\t";
			}
		print OUT "maxrate_reps\n";
		}
	$out_first_flag++;
#	print "@wells"."\n";
	my @ttls = grep {$_ ne "NA"} @_ = map {$data_hash{$_}{time_to_log}} @wells;
	my @ttls_exp = map {$data_hash{$_}{time_to_log}} @wells;
#	print "@ttls"."\n";
	my @maxods = map {$data_hash{$_}{max_ave}} @wells;
	my @rates = map {$data_hash{$_}{max_rate}} @wells;
	my @exp_stats = (scalar @wells, scalar @ttls, test(mean(@ttls)), test(stddev(@ttls)), scalar @maxods, test(mean(@maxods)), test(stddev(@maxods)), test(mean(@rates)), test(stddev(@rates)));
#					0			   1			  2					 3				      4				  5					   6					  7					  8
	my $text = $exp."\t".join("\t", @exp_stats)."\t\t".join("\t", @ttls_exp)."\t".join("\t", @maxods)."\t".join("\t", @rates);
	print OUT "$text\n"
	}

#foreach my $exp (nsort keys %experiments) {
#	my @wells = grep { $meta_hash{$_}{all} eq $exp } nsort keys %meta_hash;
#	foreach my $well (@wells){
#		my ($max_rate, $max_rate_time) = calculate_growth_rate(\@{$data_hash{$well}{curve}}, \@times, $rate_window);
#		print "Well: $well\tRate = $max_rate at t = $max_rate_time\n";
#		}
#	}

#plotting
if ($plot_flag) {
	print "\tPlotting curves with ggplot2 (R)...\n";
	print "\t\tHeader Lines = $header_lines\n\t\tData Lines = $data_lines\n";
	my $plotted_lines = $data_lines + $gap_lines;
	if ($format_flag) {
		system("$CURVE_PLOTTER $dirs$infile $groupfile $log_threshold T 0 $plotted_lines");
		}
	else {
		system("$CURVE_PLOTTER $dirs$infile $groupfile $log_threshold F $header_lines $plotted_lines");
#		print	"$CURVE_PLOTTER $dirs$infile $groupfile $log_threshold F $header_lines $plotted_lines";
		}
	}
#print "Complete.\n";

sub file_format_check {
	my $file = shift;
	my $text = `file $file`;
	unless ($text =~ /ASCII/) {
		die "Text encoding wrong, check for degree symbols (for example) and fix before re-run!\n";
		}
	}
	
sub format_eon_file {
	my $file = shift;
	my $formatted_file_name = $file =~ s/(\..+$)/_formatted$1/r;
	system("tr -d '\15\32' < $file > $formatted_file_name");
	system("perl -i -pe 's/\x{B0}//g' $formatted_file_name");
	file_format_check($formatted_file_name);
	print "File format corrected, proceeding with analysis...\n";
	return($formatted_file_name);
	}

sub read_formatted_input { #parses formatted input file (col1 = time(hr), coln = header + curve data
	my $infile1 = shift;
	my %data_hash = ();
	print "\tReading user-formatted input file $infile\n";
	#grab headers and data from input file (from plate reader, semi-formatted)
	open (IN, "<$infile1") or die "ERROR: Can't open $infile1: $!\n";
	my $header_flag = 0;
	my @headers = ();
	my @times = ();
	my $data_points = 0;
	my $NA_lines = 0;
	my $max_time = 0;
	while (my $line = <IN>) {	
		$line =~ s/\R//g;
		chomp $line;
		my @line_array = ();
		if ($header_flag == 0) {
			@headers = split ("\t", $line);
			for (my $i = 1; $i < @headers; $i++) {
				$headers[$i] =~ s/\s/_/;
#				print "My header is $headers[$i]\n";
				}
			$header_flag++;
			}
		else {
			if ($line =~ /NA/) {
#				print "\t\tFound line with \"NA\" values.  Skipping for analysis.\n";
				$NA_lines++;
				}
			elsif ($line =~ /^\#/) {
				print "\t\tCommented line skipped.\n";
				}
			else {
				@line_array = split ("\t", $line);
				for (my $j = 0; $j < @line_array; $j++) {
					if ($j == 0) {
						push(@times, $line_array[$j]);
						$max_time = $line_array[$j];
						}
					else {
						push(@{$data_hash{$headers[$j]}{"curve"}}, $line_array[$j]);
						}
					}
				$data_points++;
				}
			}
		}
	close IN;
	return ($data_points, \%data_hash, \@times, $NA_lines, $max_time);
	}

sub read_raw_input {	#parses raw, unformatted gen5 exported file
	my $infile1 = shift;
	my %data_hash = ();
	print "\tReading raw input file $infile\n";
	#grab headers and data from input file (from plate reader, semi-formatted)
	open (IN, "<$infile") or die "ERROR: Can't open $infile: $!\n";
	my $header_flag = 0;
	my @headers = ();
	my @times = ();
	my $data_points = 0;
	my $delay = 0;
	my $header_lines = 0;
	my $NA_lines = 0;
	my $line_counter = 0;
	my $max_time = 0;
	while (my $line = <IN>) {	
#		print LOG "Delay: $delay\n";
		$line =~ s/\R//g;
		chomp $line;
		$line_counter++;
		if ($line =~ /^Time\tT/) {
#			print LOG "Found header for data to follow!\n";
			$header_lines = $line_counter - 1;
			$delay++;
			}
		if ($delay == 1) {
			my @line_array = ();
			if ($header_flag == 0) {
#				print LOG "Header line\n";
				@headers = split ("\t", $line);
				for (my $i = 2; $i < @headers; $i++) {
					$headers[$i] =~ s/\s/_/;
#					print "My header is $headers[$i]\n";
					}
				$header_flag++;
				}
			else {
				if ($line =~ /NA/) {
#					print "Found line with \"NA\" values.  Skipping for analysis.\n";
					$NA_lines++;
					}
				elsif ($line =~ /^\#/) {
					print "\t\tCommented line skipped.\n";
					}
				else {
#					print "$line\n";
					@line_array = split ("\t", $line);
					my $prev = $data_points-1;
#					print LOG "Data Line: $data_points\t";
					for (my $j = 0; $j < @line_array; $j++) {
						if ($j == 0) {
							my $processed_time = time_processor($line_array[$j]);
#							print LOG "Processed Time: $processed_time\t";
#							print LOG "(Now: $processed_time, Prev: $times[$prev])\n";
							if ($prev < 0) {
								push(@times, $processed_time);
								}
							elsif ($processed_time > $times[$prev]) {
#								print LOG "Times check out!\n";
								push(@times, $processed_time);
								$max_time = $processed_time;
#								print LOG "@times\n";
								}
							else {
#								print LOG "Current line time is less than previous!\n";
								$delay++;
								}
							}
						elsif ($j > 1) {
#							print LOG "\.";
							push(@{$data_hash{$headers[$j]}{"curve"}}, $line_array[$j]);
							}
						}
					$data_points++;
#					print LOG "\n";
					}
				}
			}
		}
	close IN;
	return ($data_points, \%data_hash, \@times, $header_lines, $NA_lines, $max_time);
	}

sub get_range_ave {	#gets the average of a user-defined range of values, centered on an input array index
	my $index = shift;
	my $tmp = shift;
	my @array = @$tmp;
	my $range = shift;
	my ($ave, $counter) = 0;
	my $low = $index - $range;
	my $high = $index + $range + 1;
	for (my $i = $low; $i < $high; $i++) {
		if (exists $array[$i]) {
#			print "$array[$i]\t";
			$ave += $array[$i];
			$counter++;
			}
		}
#	print "\n";
	$ave = $ave/$counter;
	return $ave;
	}

sub calculate_growth_rate {
	my $tmp1 = $_[0];
	my $tmp2 = $_[1];
	my $window = $_[2];
	my @curve = @$tmp1;
	my @time = @$tmp2;
	my @log_curve = log2(@curve);
	my @rates = ();
	for (my $i = $window; $i < @time-$window; $i++) {
		my $rate = ($log_curve[$i+$window] - $log_curve[$i-$window]) / ($time[$i+$window] - $time[$i-$window]);
		push(@rates, $rate);
		}
#	print "@rates\n";
	my $max_rate = max @rates;
	my $max_rate_time = $times[firstidx {$_ == $max_rate} @rates];
	return $max_rate, $max_rate_time;
	}
		
sub test {	#rounding helper sub.  prevents errors when trying to round an n/a above.
	my $val = shift;
	unless ($val eq "n/a") {
		$val = round($val, 3);
		}
	return $val;
	}

sub time_processor {	#converts hr:min:sec formatted time data to hour fractions.
	my $time = shift;
#	print LOG "Unprocessed Time: $time\n";
	my @time_array = split(":", $time);
#	print LOG "Time Array = @time_array\n";
	my $frac = ($time_array[1]+$time_array[2]/60)/60;
	$time = $time_array[0]+$frac;
	return $time;
	}

sub log2 { #from http://davetang.org/muse/2011/04/06/using-perl-to-log-transform-data/
   my @n = @_;
   my @t = ();
   foreach my $n (@n){
      if ($n == 0){
         die "OD600 of 0 detected, cannot log-transform!\n";
      }
      my $t = log($n)/log(2);
      #rounded to two decimal places
      $t = sprintf("%.2f",$t);
      push(@t,$t);
   }
   return(@t);
}

sub show_help {	#help/usage sub
	my $error = shift;
	print "\n\t$error\n" if $error;
	print "\n\tperl growth_curve_analyzer.pl -i <infile> -g <groupfile>\n";
	print "\t\t-i <input file, formatted as output from Biotek Eon>\n";
	print "\t\t-m <group/metadata file, tab-formatted, with well, org, medium, and trt info columns>\n";
	print "\t\t-p <use R script to plot growth curves for organism x treatment> OPTIONAL, default is FALSE\n";	
	print "\t\t-o <output filename, with suffix> OPTIONAL\n";
	print "\t\t-t <OD threshold for log phase, user-defined> OPTIONAL\n";
	print "\t\t-r <range for max OD averaging, as in max +/- n is (max OD range)> OPTIONAL\n";
	print "\t\t-u <input files are user formatted, not raw off the gen5 software>, default is \"raw\"\n";
	print "\t\t-f <fix file formatting and proceed with analysis>\n";
	print "\t\t-g <gaps in data will be interpolated based on time and plotted in dashed sections>\n";
	print "\t\t-h (print help and exit)\n";
	print "\n\t\tEx: perl growth_curve_analyzer.pl -i input_curves.txt -g groups.txt -o curve_metrics.txt -t 0.3 -r 1\n\n";
	exit(1);
	}
